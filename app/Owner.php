<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    protected $table = 'owners';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname'
    ];

    public function dogs() {
        return $this->belongsToMany('App\Owner', 'owners_x_dogs', 'owner_id', 'dog_id');
    }
}
