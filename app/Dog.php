<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dog extends Model
{
    protected $table = 'dogs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'color'
    ];

    public function owners() {
        return $this->belongsToMany('App\Owner', 'owners_x_dogs', 'dog_id', 'owner_id');
    }
}
