@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="w-100">
                            Dogs
                            <span class="pull-right">
                                <a href='{{ route('dogs.create') }}' class="btn btn-outline-success">Add New</a>
                            </span>
                        </h3>

                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($dogs->count())
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Color</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($dogs as $dog)
                                    <tr>
                                        <td>{{ $dog->id }}</td>
                                        <td>{{ $dog->name }}</td>
                                        <td>{{ $dog->colour }}</td>
                                        <td>
                                            <a href='{{ route('dogs.show', $dog->id) }}' class="btn btn-outline-info">Show</a>
                                            <a href='{{ route('dogs.edit', $dog->id) }}' class="btn btn-outline-primary">Edit</a>
                                            <a href='{{ route('dogs.destroy', $dog->id) }}' class="btn btn-outline-danger">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning" role="alert">
                                No dogs in the list yet!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
