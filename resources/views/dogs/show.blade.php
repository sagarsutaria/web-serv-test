@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="w-100">
                        {{ $dog->name }}
                        <span class="pull-right">
                                <a href='{{ route('dogs.create') }}' class="btn btn-outline-success">Add New</a>
                            </span>
                    </h3>

                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        @if ($dog)
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th>Id</th>
                                    <td>{{ $dog->id }}</td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $dog->name }}</td>
                                </tr>
                                <tr>
                                    <th>Color</th>
                                    <td>{{ $dog->colour }}</td>
                                </tr>
                                <tr>
                                    <th>Actions</th>
                                    <td>
                                        <a href='{{ route('dogs.edit', $dog->id) }}' class="btn btn-outline-primary">Edit</a>
                                        <a href='{{ route('dogs.destroy', $dog->id) }}' class="btn btn-outline-danger">Delete</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
